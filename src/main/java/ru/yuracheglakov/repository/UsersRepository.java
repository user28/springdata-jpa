package ru.yuracheglakov.repository;

import org.springframework.data.repository.CrudRepository;
import ru.yuracheglakov.entities.UsersEntity;

import java.util.List;

public interface UsersRepository extends CrudRepository<UsersEntity, Integer> {
    List<UsersEntity> findByFirstName(String firstName);

    List<UsersEntity> findByFirstNameAndLastName(String firstName, String lastName);
}
