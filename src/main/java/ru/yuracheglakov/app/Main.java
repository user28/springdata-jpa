package ru.yuracheglakov.app;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.yuracheglakov.Config;
import ru.yuracheglakov.entities.UsersEntity;
import ru.yuracheglakov.services.UsersService;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext(Config.class);

        UsersService service = annotationConfigApplicationContext.getBean("jpaUsersService", UsersService.class);
        List<UsersEntity> contacts = service.findAll();
        printAll(contacts);

        contacts = service.findByFirstName("Yura");
        printAll(contacts);

        contacts = service.findByFirstNameAndLastName("Yura", "Cheglakov");
        printAll(contacts);
    }

    private static void printAll(List<UsersEntity> contacts) {
        System.out.println("printAll: ");
        for (UsersEntity contact : contacts) {
            System.out.println(contact.getFirstName());
        }
    }
}
