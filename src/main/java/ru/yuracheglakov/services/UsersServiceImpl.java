package ru.yuracheglakov.services;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.yuracheglakov.entities.UsersEntity;
import ru.yuracheglakov.repository.UsersRepository;

import java.util.List;

@Controller("jpaUsersService")
@Repository
@Component
@Transactional
public class UsersServiceImpl implements UsersService {

    @Autowired
    private UsersRepository usersRepository;

    public List<UsersEntity> findAll() {
        return Lists.newArrayList(usersRepository.findAll());
    }

    public List<UsersEntity> findByFirstName(String firstName) {
        return Lists.newArrayList(usersRepository.findByFirstName(firstName));
    }

    public List<UsersEntity> findByFirstNameAndLastName(String firstName, String lastName) {
        return Lists.newArrayList(usersRepository.findByFirstNameAndLastName(firstName, lastName));
    }
}
