package ru.yuracheglakov.services;

import ru.yuracheglakov.entities.UsersEntity;

import java.util.List;

public interface UsersService {
    List<UsersEntity> findAll();

    List<UsersEntity> findByFirstName(String firstName);

    List<UsersEntity> findByFirstNameAndLastName(String firstName, String lastName);
}
